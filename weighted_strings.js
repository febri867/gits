function weightedUniStrings(string, queries) {
    let dict = [];
    let alphabets = 'abcdefghijklmnopqrstuvwxyz'.split('');
    for(let j =0;j<alphabets.length;j++){
        dict.push({
            key:alphabets[j],
            value:j+1
        })
    }
    let previous = '';
    let newQuery = [];
    for(let i = 0;i<string.length;i++){
        let weight = 0;
        if(previous.includes(string[i])){
            previous = previous+string[i];
            for(let k = 0;k<dict.length;k++){
                if(dict[k].key === string[i]){
                    weight =dict[k].value *previous.length;
                    newQuery.push(weight);
                }
            }
        }else{
            previous = string[i];
            for(let k = 0;k<dict.length;k++){
                if(dict[k].key === string[i]){
                    weight =dict[k].value;
                    newQuery.push(weight);
                }
            }
        }
    }
    let result = [];
    for(let z =0; z<queries.length ;z++){
        if(newQuery.includes(queries[z])){
            result.push("Yes")
        }
        else{
            result.push("No");
        }
    }
    return result;
}

console.log(weightedUniStrings('abbcccd', [1, 3, 9, 8]))