function isPalindrome(x){
    x = x.toString().split('');
    let len = x.length;
    for(let i=0; i<len/2;i++){
        if(x[i] !== x[len-1-i]){
            return false;
        }
    }
    return true;
}
function highestPalindrome(str, k){
    let highest = 0;
    for(let i=str; i>=k; i--){
        for(let j=str; j>=k; j--){
            let result = i*j;
            if(result < highest) break;
            if(isPalindrome(result) && result > highest){
                highest = result;
            }
        }
    }
    return highest;
}

console.log(highestPalindrome(3943, 1))