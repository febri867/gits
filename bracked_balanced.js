function isBracketBalanced(bracket)
{

    let temp = [];

    for(let i = 0; i < bracket.length; i++)
    {
        let x = bracket[i];

        if (x === '(' || x === '[' || x === '{')
        {
            temp.push(x);
            continue;
        }

        if (temp.length === 0)
            return false;

        let check;
        switch (x){
            case ')':
                check = temp.pop();
                if (check === '{' || check === '[')
                    return false;
                break;

            case '}':
                check = temp.pop();
                if (check === '(' || check === '[')
                    return false;
                break;

            case ']':
                check = temp.pop();
                if (check === '(' || check === '{')
                    return false;
                break;
        }
    }
    return (temp.length === 0 ? 'YES' : 'NO');
}

console.log(isBracketBalanced('{ [ ( ) ] }'))